<?php 
	//session_start();
    require_once("cabecalho.php");
    include("classes/conexao.php"); 
    $id_usuario = $_SESSION['id_usuario'];
    //definir o numero de itens por página
    $itens_por_pagina = 15;
    $pagina=1;
    if(isset($_GET['pagina']))
        $pagina = intval($_GET['pagina']);
    $exibe = (($pagina-1)*$itens_por_pagina);
    
    //puxar produtos do banco
  
    $sql = "SELECT id_interesse,P2.nome,P2.id FROM produtos P1, produtos P2, usuarios prop, usuarios u, interesses i WHERE u.id_usuario=$id_usuario AND P2.proprietario = $id_usuario AND P2.id = i.id_prod1 AND P1.id = i.id_prod2 AND prop.id_usuario = i.id_usu2 AND u.id_usuario = i.id_usu1" ;

    $execute = $mysqli->query($sql) or die($mysqli->error);
    $produto = $execute->fetch_assoc();

    $sql2= "SELECT P1.nome, P1.resumo, P1.id FROM produtos P1, produtos P2, usuarios prop, usuarios u, interesses i WHERE u.id_usuario=$id_usuario AND P2.proprietario = $id_usuario AND P2.id = i.id_prod1 AND P1.id = i.id_prod2 AND prop.id_usuario = i.id_usu2 AND u.id_usuario = i.id_usu1";
    $execute2 = $mysqli->query($sql2) or die($mysqli->error);
    $produto2 = $execute2->fetch_assoc();

    $sql3 = "SELECT prop.nome, prop.email, prop.telefone FROM produtos P1, produtos P2, usuarios prop, usuarios u, interesses i WHERE u.id_usuario=$id_usuario AND P2.proprietario = $id_usuario AND P2.id = i.id_prod1 AND P1.id = i.id_prod2 AND prop.id_usuario = i.id_usu2 AND u.id_usuario = i.id_usu1";
    $execute3 = $mysqli->query($sql3) or die($mysqli->error);
    $produto3 = $execute3->fetch_assoc();
    
    $num = $execute->num_rows;

    //Pega a quantidade total de objetos no banco de dados
    $num_total = $mysqli->query("SELECT id,nome,descricao FROM produtos WHERE proprietario='$id_usuario'")->num_rows;

    //definir numero de páginas
    $num_paginas = ceil($num_total / $itens_por_pagina);
    
?>
<!DOCTYPE html>
<html>
	<head>
       <meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="estilo.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	
	<body>
        <?php
            
            if(isset($_SESSION['usuario'])){         
        ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10">
                    <h1>Propostas</h1>
                    <?php //if($num > 0){ ?>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>Meu Produto</td>
                                <td>Produto Oferecido</td>
                                <td>Resumo</td>
                                <td>Nome do Ofertante</td>
                                <td>Email</td>
                                <td>Telefone</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php do{ ?>
                            <tr>
                                <td><?php echo $produto['nome']; ?></td>
                                <td><?php echo $produto2['nome']; ?></td>
                                <td><?php echo $produto2['resumo']; ?></td>
                                <td><?php echo $produto3['nome']; ?></td>
                                <td><?php echo $produto3['email']; ?></td>
                                <td><?php echo $produto3['telefone']; ?></td>
                                <?php if (isset($produto)){?>
                                <td><a href="aceitar.php?id_prod1=<?=$produto['id']?>&id_prod2=<?=$produto2['id']?>&id3=<?=$produto['id_interesse']?>"><button type="button" class="btn btn-success">Aceitar</button></td></a>
                                <td><a href="recusar.php?id3=<?=$produto['id_interesse']?>"><button type="button" class="btn btn-danger">Recusar</button></td></a>
                                <?php } ?>
                            </tr>
                    <?php if (!isset($produto2)){ ?>
            <tr>
                <td colspan="6">Você não possui ofertas ainda</td>
            </tr>
        <?php } ?>
                                    
                            <?php } while($produto = $execute->fetch_assoc()); ?>
                        </tbody>
                    </table>

                    <nav>
                          <ul class="pagination">
                            <li>
                              <a href="meusProdutos.php?pagina=1" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                              </a>
                            </li>
                            <?php for($i=0;$i<$num_paginas;$i++){ ?>
                              <li> <a href="meusProdutos.php?pagina=<?php echo $i+1; ?>"> <?php echo $i+1; ?> </a> </li>
                            <?php } ?>
                            <li>
                              <a href="meusProdutos.php?pagina=<?php echo $num_paginas; ?>" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                              </a>
                            </li>
                          </ul>
                        </nav>
                    <?php// } ?>
                </div>
            </div>
        </div>
        <?php require_once("footer.php"); ?>
        <?php }else{
                ?><script> alert("Efetue o Login");window.location.href="index.php";</script>
        <?php
            }
        ?> 
    </body> 
</html>