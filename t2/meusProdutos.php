<?php
	//session_start();
    require_once("cabecalho.php");
    include("classes/conexao.php"); 
    $id_usuario = $_SESSION['id_usuario'];
    //definir o numero de itens por página
    $itens_por_pagina = 15;
    $pagina=1;
    if(isset($_GET['pagina']))
        $pagina = intval($_GET['pagina']);
    $exibe = (($pagina-1)*$itens_por_pagina);
    
    //puxar produtos do banco
    $sqlcode = "SELECT * FROM produtos where proprietario='$id_usuario'"; 
    $execute = $mysqli->query($sqlcode) or die($mysqli->error);
    $produto = $execute->fetch_assoc();
    $num = $execute->num_rows;

    //Pega a quantidade total de objetos no banco de dados
    $num_total = $mysqli->query("SELECT id,nome,descricao FROM produtos WHERE proprietario='$id_usuario'")->num_rows;

    //definir numero de páginas
    $num_paginas = ceil($num_total / $itens_por_pagina);
    
?>
<script>
    function confirmar(id) {
         var resposta = confirm("Deseja remover esse registro?");

         if (resposta == true) {
              window.location.href = "excluirProd.php?id="+id;
         }
    }
</script>
<!DOCTYPE html>
<html>
	<head>
	
       <meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="estilo.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	
	<body>
        
		<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
	</br>
		<div class="container">
			 <?php
			
            if(isset($_SESSION['usuario'])){            
        ?>
       
		<div class="row">
			<div class="col-lg-10">
				<h1>Produtos</h1>
				<?php //if($num > 0){ ?>
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<td>Nome</td>
							<td>Descrição</td>
							<td>Resumo</td>
						</tr>
					</thead>
					<tbody>
						<?php do{ ?>
						<tr>
							<td><?php echo $produto['nome']; ?></td>
							<td><?php echo $produto['descricao']; ?></td>
							<td><?php echo $produto['resumo']; ?></td>
							<td><button onclick="confirmar(<?=$produto['id']?>)">Excluir</button></td>
						</tr>
						<?php } while($produto = $execute->fetch_assoc()); ?>
					</tbody>
				</table>

				<nav>
					  <ul class="pagination">
						<li>
						  <a href="meusProdutos.php?pagina=1" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						  </a>
						</li>
						<?php for($i=0;$i<$num_paginas;$i++){ ?>
						  <li> <a href="meusProdutos.php?pagina=<?php echo $i+1; ?>"> <?php echo $i+1; ?> </a> </li>
						<?php } ?>
						<li>
						  <a href="meusProdutos.php?pagina=<?php echo $num_paginas; ?>" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						  </a>
						</li>
					  </ul>
					</nav>
				<?php// } ?>
			</div>
		</div>
              
        <?php }else{
                ?><script> alert("Efetue o Login");window.location.href="index.php";</script>
        <?php
            }
			 
        ?> 
		</div>
		<?php require_once("footer.php"); ?>
	</div>

        
		
       
    </body> 
</html>