
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="estilo.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
	<?php require 'cabecalho.php'?>
	<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 container">		
		<div class="card card-container">
			<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
			<p id="profile-name" class="profile-name-card"></p>
			<form class="form-signin" action="validarLogin.php" method="POST">
				<span id="reauth-email" class="reauth-email"></span>
				<input name="usuario" id="inputEmail" class="form-control" placeholder="Usuário" required autofocus>

				<input type="password" name="senha" id="inputPassword" class="form-control" placeholder="Senha" required>
	
				<div id="remember" class="checkbox">
					<label>
						<input type="checkbox" value="remember-me"> Lembrar me
					</label>
				</div>
				<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Entrar</button>
				<?php					
					if(isset($_SESSION['msg']))
					{
						print_r($_SESSION['msg']);
					}
				?>	
			</form><!-- /form -->
		</div><!-- /card-container -->
	</div><!-- /container -->
	<?php require 'footer.php'?>
</body>
</html>