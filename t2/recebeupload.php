<?php
    include("classes/conexao.php"); 
	require_once("cabecalho.php");
    $nome_prod=$_POST['nomeprod'];
    $descricao=$_POST['descprod'];
    $resumo=$_POST['resumoprod'];
    $id_usuario = $_SESSION['id_usuario'];
/******
 * Upload de imagens
 ******/
 
// verifica se foi enviado um arquivo
if ( isset( $_FILES[ 'foto' ][ 'name' ] ) && $_FILES[ 'foto' ][ 'error' ] == 0 ) {
   $msg1 = 'Você enviou o arquivo: <strong>' . $_FILES[ 'foto' ][ 'name' ] . '</strong><br />';
   $msg2 = 'Este arquivo é do tipo: <strong > ' . $_FILES[ 'foto' ][ 'type' ] . ' </strong ><br />';
   $msg3 = 'O nome do seu produto é:<strong>' . $nome_prod . '</strong><br />';
   $msg4 = 'Descrição do seu produto: <strong>' . $descricao . '<br /><br />';

    $arquivo_tmp = $_FILES[ 'foto' ][ 'tmp_name' ];
    $nome = $_FILES[ 'foto' ][ 'name' ];
    
    // Pega a extensão
    $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );
    $extensao1 = '.' . $extensao;
    // Converte a extensão para minúsculo
    $extensao2 = strtolower ( $extensao1 );
 
    // Somente imagens, .jpg;.jpeg;
    // Aqui eu enfileiro as extensões permitidas e separo por ';'
    // Isso serve apenas para eu poder pesquisar dentro desta String
    if ( strstr ( '.jpg;.jpeg;.png;.gif', $extensao2 ) ) {
        // Cria um nome único para esta imagem
        // Evita que duplique as imagens no servidor.
        // Evita nomes com acentos, espaços e caracteres não alfanuméricos
        $novoNome = uniqid ( time () ) . $extensao2;
 
        // Concatena a pasta com o nome
        $destino = 'img/' . $novoNome;
 
        // tenta mover o arquivo para o destino
        if ( copy ( $arquivo_tmp, $destino ) ) {
            $msg5 = 'Arquivo salvo com sucesso !</strong><br/>';
            ////////////// SQL//////////////////
            $sql = "INSERT INTO produtos (nome,descricao,resumo,nome_img,proprietario) VALUES('$nome_prod','$descricao','$resumo','$novoNome','$id_usuario')";
            $execute = $mysqli->query($sql) or die($mysqli->error);
            
        }
        else
            $msg7 = 'Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita.<br/>';
    }
    else
        $msg8 = 'Você poderá enviar apenas arquivos "*.jpg;*.jpeg;*.png;*.gif;"<br/>';
}
else
    $msg9 = 'Você não enviou nenhum arquivo!';
?>

<!DOCTYPE html>
<html>
	<head>
        <meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="estilo.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	</head>
	
	<body>
        
		<div class ="registroprod2">
			<?php  
				if(isset($msg7))
				{                       
					echo $msg7;
				}
				else if(isset($msg8))
				{    
					echo $msg8;
				
				}
				else if(isset($msg9))
				{                        
						
					echo $msg9;

				}
				else
				{
					echo $msg1;
					echo $msg2;
					echo $msg3;
					echo $msg4;
					echo $msg5;
					//echo $sql;
					
				}

			?>
			
			<img src="<?php echo $destino ?>" style="max-width: 500px; max-height: 500"/>
			
		</div>
    </body>
</html>