<?php 
	include("classes/conexao.php"); 
	require_once("cabecalho.php");
    if(isset($_GET['idProduto']))
        $id = intval($_GET['idProduto']);
	else
		header('Location: index.php');
	
	$sqlcode = "SELECT * FROM produtos WHERE id=$id "; 
    $execute = $mysqli->query($sqlcode) or die($mysqli->error);
    $produto = $execute->fetch_assoc();
    $i=0;
    if (isset($_SESSION['id_usuario'])){
        $sql = "SELECT id,nome FROM produtos WHERE proprietario = $_SESSION[id_usuario]";
        $execute = $mysqli->query($sql) or die($mysqli->error);
        
        while($sql = mysqli_fetch_array($execute)){
            $id_prod[$i] = $sql["id"];
            $nome_prod[$i] = $sql["nome"];
            $i++;
        }
    }

?>
<html>
	<head>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<link rel="stylesheet" href="estilo.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	</head>
	
	<body>
        
	<script>
		function confirmar(){
			alert("Oferta realizada com sucesso!");
		}
	</script>

		<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			</br>
			<div class="container promo-box-page">
				<div class="row category-child col-lg-12 col-md-12 col-sm-12 col-xs-12 catalogo" >
  
					<div class="media ">
				  
						<div class="media-body bg-success" style="text-align: center">
					  
							<h2 class="media-bottom"><?php echo $produto['nome'];?></h2>
							<p><?php echo $produto['descricao'];?></p>
							<div class="media-center media-middle">
								<a href="#">
								  <img class="media-object" style="max-width: 350px; max-height: 350; display: block; margin: 0 auto;" src="img/<?php echo $produto['nome_img']; ?>" alt="img/$produto['nome_img']; ?>">
								</a>
							</div>
							<br><br><br>
							<?php if (!isset($_SESSION['id_usuario'])){ ?>
							  <div class="modal-body">
								  <a href="minhasOfertas.php"><button disabled type="button" class="btn btn-info btn-lg" data-toggle="modal" id="btn_interesse" data-target="#myModal">Checar ofertas!</button></a>
								  <p style="float:0;font-size:14px;">Para concluir esta ação, é necessário realizar login antes</p>            
							  </div>
							<?php } else if ($_SESSION['id_usuario'] != $produto['proprietario']) { ?>
						  
							<button type="button" class="btn btn-info btn-lg" data-toggle="modal" id="btn_interesse" data-target="#myModal">Me interessei</button>
							<br>
								<div id="myModal" class="modal fade" role="dialog">
								  <div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title" style="font-size:20px; color:black; font-weight:bold;">Selecione produto que deseja trocar</h4>
										</div>
										<div class="modal-body">
											<form action="proposta.php" type="post">
												<?php
													$c=0;
													foreach($nome_prod as $nomeP){ ?>
													<input type="radio" name="id_my_prod" value ="<?=$id_prod[$c]?>">
												<span class="text-pop"><?php echo "$nomeP"?></span><br> 
												
												<?php $c++;}?>
												<input type="hidden" name="idP1" value="<?=$produto['id']?>"/>
												<input type="hidden" name="idU" value="<?=$produto['proprietario']?>"/>
												<br><input onclick="confirmar();" class="btn btn-success btn-sm" type="submit" value="Confirmar">
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Fechar</button>
										</div>
									</div>
								</div>
							  </div>
						  </div>
							<?php } else {  ?>
								<a href="minhasOfertas.php"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" id="btn_interesse" data-target="#myModal">Checar ofertas!</button></a>
								<p style="float:0;font-size:14px;"> (Este produto pertence à você)</p>
							 <?php } ?>    
									
					  </div>
                    </div>
                </div> 
			</div>
		</div>	
   
	   <div class = "row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="baixo">
			<?php require 'footer.php'?>
		</div>
	</body>
</html>