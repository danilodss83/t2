<?php 
    include("classes/conexao.php"); 
	require_once("cabecalho.php");
	//definir o numero de itens por página
    $itens_por_pagina = 10;
    $pagina=1;
    if(isset($_GET['pagina']))
        $pagina = intval($_GET['pagina']);
    $exibe = (($pagina-1)*$itens_por_pagina);
    
    //puxar produtos do banco
    $sqlcode = "SELECT * FROM produtos LIMIT $exibe,$itens_por_pagina"; 
    $execute = $mysqli->query($sqlcode) or die($mysqli->error);
    $produto = $execute->fetch_assoc();
    $num = $execute->num_rows;

    //Pega a quantidade total de objetos no banco de dados
    $num_total = $mysqli->query("SELECT id,nome,descricao FROM produtos")->num_rows;

    //definir numero de páginas
    $num_paginas = ceil($num_total / $itens_por_pagina);
    
?>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="estilo.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script>
		$(function() {
			$('span').click(function(){
			var id = $(this).find('input').val();
			window.location = 'descricao.php?id=' + id;
			
		});
		
  $("#produto").click(function(){
			var id = $("#produtoId").val();
			//window.location = 'http://localhost/t2/produtoDetalhe.php?id=' + id;
			window.open('http://localhost/t2/produto.php?idProduto=' + id);
			
		});
});
		</script>
		
	</head>
<body>
	
	<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
	</br>
		<div class="container promo-box-page">
			<div class="row category-child col-lg-12 col-md-12 col-sm-12 col-xs-12 catalogo" >
				<!--produtos-->
				<?php do{ ?>
				   <div class='col-lg-2 col-md-4 col-xs-6 thumb '>
						<span>
							<a class='thumbnail' data-toggle='modal' data-target='#myModal' href='#'>
							<img class='img-responsive' src="img/<?php echo $produto['nome_img']; ?>" alt="<?php echo $produto['nome_img'] ?>">
							<div class='wrapper'>
								<div class='caption post-content'>
									<span><?php echo $produto['nome']?></span>
									<input type="hidden" value="<?php echo $produto['id']; ?>"/>
								</div>
							</div>
							</a>
						<span>
					</div>
		
				<?php } while($produto = $execute->fetch_assoc());?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div id="myModal" class="modal fade in">
					<div class="modal-dialog">
						<div class="modal-content">
							 <div class="modal-body">
								<h4>Descrição do Produto</h4>
								<p>																			
									

									<?php
										if(isset($id))
										{
											
											echo "<script type='text/javascript'>											
													$('#myModal').modal('show');											
												</script>";
											echo "$x";
											echo "<input type='hidden' id='produtoId' value=".$id.">";
										}											
									?>
																										
								</p>
								
								
								
							</div>
							<div class="modal-footer">
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
								
	</script>
									<button class="btn btn-sm btn-primary " id="produto" type="button">Verificar Ofertas</button>
								</div>	
								<div class="btn-group text-center">	
									<button type="button" class="btn btn-sm btn-success" data-dismiss="modal">Fechar</button>
								</div>
							</div>
			 
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dalog -->
				</div><!-- /.modal -->
				
			</div>				
		</div>
	</div>
	<div class = "row col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class = "row col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
		<div class = "row col-lg-4 col-md-4 col-sm-4 col-xs-4">		
			<nav>
			  <ul class="pagination">
				<li>
				  <a href="index.php?pagina=1" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
				  </a>
				</li>
				<?php for($i=0;$i<$num_paginas;$i++){ ?>
				  <li> <a href="index.php?pagina=<?php echo $i+1; ?>"> <?php echo $i+1; ?> </a> </li>
				<?php } ?>
				<li>
				  <a href="index.php?pagina=<?php echo $num_paginas; ?>" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
				  </a>
				</li>
			  </ul>
			</nav>
		</div>
	</div>
	<div class = "row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="baixo">
	<?php require 'footer.php'?>
	</div>
</body>
</html>