<!DOCTYPE html>
<html>
	<head>
        <meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="estilo.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	
	<body>

		<?php require_once("cabecalho.php");?>
		<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			</br>
			<div class="container">
				<form class="form-horizontal" action="recebeupload.php" id ="frm" enctype="multipart/form-data" method="POST">
					<fieldset>
						<!-- Form name-->
						<legend>Formulário de Cadastro de Produtos</legend>
						<!-- Text input-->
						<div class="form-group">
						  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="nome">Nome</label>  
						  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
							<input type="text" class="form-control" id="InputNomeProd" placeholder="Digite o nome do produto" name="nomeprod" maxlength="30" required>
													
						  </div>
						</div>
						
						<div class="form-group">
						  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="InputDesc">Descrição</label>  
						  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
							<textarea class="form-control" maxlength="256" rows="7" cols="55" style="resize:none" form="frm" name="descprod" placeholder="Digite a descrição do produto" required></textarea>
													
						  </div>
						</div>
					  
						<div class="form-group">
						  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="InputResumo">Resumo</label>  
						  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
							<textarea class="form-control" maxlength="256" rows="7" cols="55" id="InputResumo" style="resize:none" form="frm" name="resumoprod" placeholder="Digite um resumo do produto" required></textarea>
													
						  </div>
						</div>
						
						<div class="form-group">
						  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="nome">Selecione a imagem do produto</label>  
						  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
							<input class="form-control" type='file' name='foto' value='Cadastrar foto' required>
													
						  </div>
						</div>
						
						<div class="form-group">
						  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="enviar"></label>
						  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
							<button id="enviar" name="enviar" class="btn btn-success" type="submit">Enviar</button>
							<button id="limpar" name="limpar" class="btn btn-warning" type="reset">Limpar</button>
						  </div>
						</div>  
					</fieldset>					
				</form>
			</div>
		</div>
	<?php require 'footer.php'?>
    </body>
</html>