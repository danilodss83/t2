<?php
	session_start();	
	validaForm();
	loadSession();
	function loadSession()
	{
		$_SESSION['nome'] = $_POST['nome'];
		$_SESSION['usuario'] = $_POST['usuario'];
		$_SESSION['email'] = $_POST['email'];
		$_SESSION['senha'] = $_POST['senha'];
		$_SESSION['telefone'] = $_POST['telefone'];
	    $_SESSION['cep'] = $_POST['cep'];
	}   
	function validaEmail() {
		
		$email = $_POST['email'];
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			 return "<span class='text-danger'>Email inv&aacute;lido!</span>";			
		}			
	}
	
	function validaSenha(){
		//valicação do formato da senha
		$senha = $_POST['senha'];
		$senhaConfirma  = $_POST['confirmeSenha'];
		
		if (!preg_match("/^.*(?=.{6,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/", $senha)) { 
			return "<span class='text-danger'>Senha deve conter 6 caracteres, combinado o uso de letras com números, e entre as letras caixa baixa e alta</span>";
					 
		//validação da compatibilidade entre 'senha' e 'confirmar senha'
		} 
        if ($senha == "") 
		{
			return "<span class='text-danger'>Senha não foi alterada!</span>";			
        } 
		else if ($senha != $senhaConfirma) 
		{
			return "<span class='text-danger'> As senhas não conferem</span>";			
        } 	
	}
	
	function validaTelefone(){
		$telefone = $_POST['telefone'];
		if (!preg_match("/.*(?=.{8,9})(?=.*[0-9]).*$/", $telefone)){
			return "<span class='text-danger'>Telefone no formato incorreto.</span>";			
		}
		
	}
	
	function validaCep(){
		$cep = $_POST['cep'];
		if (!preg_match("/.*(?=.{8})(?=.*[0-9]).*$/", $cep)){
			return "<span class='text-danger'>Cep deve conter apenas 8 algarismos.</span>";			
		}
	}
	
	function validaForm()
	{
		//echo"<script>alert(' print_r($_SESSION); ');</script>";
		$_SESSION['erroEmail'] = validaEmail();
		$_SESSION['erroSenha'] = validaSenha();
		$_SESSION['erroTelefone'] = validaTelefone();
		$_SESSION['erroCep'] = validaCep();
		//echo"<script>alert(' print_r($_SESSION); ');</script>";
		if(isset($_SESSION['erroEmail']) || isset($_SESSION['erroSenha']) || isset($_SESSION['erroTelefone']) || isset($_SESSION['erroCep'])) 
		{
			//echo"<script>alert(' print_r($_SESSION); ');</script>";
			header('Location: cadastro.php');
			//return require 'cadastro.php';
		}	
		
		else
		{
			header('Location: sentMail/mailSent.php');
		}			
	}
?>